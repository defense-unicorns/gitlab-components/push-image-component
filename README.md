# push-image-component

Push a container image (using skopeo)

## Usage

Use this component to publish a container image to the gitlab registry.

You should add this component to an existing `.gitlab-ci.yml` file by using the `include:`
keyword.

```yaml
include:
  - component: ${CI_SERVER_HOST}/${GITLAB_COMPONENT_GROUP}/push-image-component/skopeo@<VERSION>
    inputs:
      stage: push-image
      image: quay.io/skopeo/stable:v1.13.3
      artifact-expiry: 1 day
      digest-artifact: "${ARTIFACT_FOLDER}/registry-digest"
      docker-image-tar: "${IMAGE_TAR_PATH}"
```

where `<VERSION>` is the latest released tag or `main`.

where `${GITLAB_COMPONENT_GROUP}` is a CICD variable with the path to the group containing the gitlab component definitions.

This adds a job called `push-image` to the pipeline.

### Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage` | `push-image`      | Optional name to provide for the stage |
| `needs` | ``      | Provide name of a dependency to complete before the stage can run |
| `image` | `quay.io/skopeo/stable:v1.13.3` | Image used for job to run |
| `artifact-expiry` | `1 day` | Artifact expiration time |
| `digest-artifact` | `${ARTIFACT_FOLDER}/registry-digest` | Path to the location of the image URI digest artifact |
| `docker-image-tar` | `${ARTIFACT_FOLDER}/image-as-tar/${CI_PROJECT_NAME}-${CI_COMMIT_SHORT_SHA}.tar` | The locally built tarball of the docker image to push  |

## Contribute

Please read about CI/CD components and best practices at: https://docs.gitlab.com/ee/ci/components 
